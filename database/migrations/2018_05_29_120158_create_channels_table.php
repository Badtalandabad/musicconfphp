<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `channels` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `supplier_id` INT(10) UNSIGNED NOT NULL,
                `id_on_supplier_side` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `artist_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                `name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
                PRIMARY KEY (`id`),
                INDEX `FK_channels_music_suppliers` (`supplier_id`),
                INDEX `FK_channels_artists` (`artist_id`),
                CONSTRAINT `FK_channels_artists` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`),
                CONSTRAINT `FK_channels_music_suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `music_suppliers` (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
