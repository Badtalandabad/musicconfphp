<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTracksArtistsMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `tracks_artists_map` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `track_id` INT(10) UNSIGNED NOT NULL,
                `artist_id` INT(10) UNSIGNED NOT NULL,
                PRIMARY KEY (`id`),
                INDEX `FK_tracks_artists_map_tracks` (`track_id`),
                INDEX `FK_tracks_artists_map_artists` (`artist_id`),
                CONSTRAINT `FK_tracks_artists_map_artists` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`),
                CONSTRAINT `FK_tracks_artists_map_tracks` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks_artists_map');
    }
}
