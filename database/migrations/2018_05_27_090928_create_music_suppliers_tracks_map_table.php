<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMusicSuppliersTracksMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `music_suppliers_tracks_map` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `supplier_id` INT(10) UNSIGNED NOT NULL,
                `track_id` INT(10) UNSIGNED NOT NULL,
                `id_on_supplier_side` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
                `channel_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                PRIMARY KEY (`id`),
                INDEX `FK_music_suppliers_tracks_map_music_suppliers` (`supplier_id`),
                INDEX `FK_music_suppliers_tracks_map_tracks` (`track_id`),
                INDEX `FK_music_suppliers_tracks_map_channels` (`channel_id`),
                CONSTRAINT `FK_music_suppliers_tracks_map_channels` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`),
                CONSTRAINT `FK_music_suppliers_tracks_map_music_suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `music_suppliers` (`id`),
                CONSTRAINT `FK_music_suppliers_tracks_map_tracks` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_suppliers_tracks_map');
    }
}
