<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTrackTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `track_tags` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `track_id` INT(10) UNSIGNED NOT NULL,
                `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `type` ENUM('category','genre','other') NOT NULL COLLATE 'utf8mb4_unicode_ci',
                PRIMARY KEY (`id`),
                INDEX `FK_track_tags_tracks` (`track_id`),
                CONSTRAINT `FK_track_tags_tracks` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_tags');
    }
}
