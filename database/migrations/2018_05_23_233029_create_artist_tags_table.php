<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateArtistTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `artist_tags` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `artist_id` INT(10) UNSIGNED NOT NULL,
                `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `type` ENUM('genre','other') NOT NULL COLLATE 'utf8mb4_unicode_ci',
                PRIMARY KEY (`id`),
                INDEX `FK_artist_tags_artists` (`artist_id`),
                CONSTRAINT `FK_artist_tags_artists` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_tags');
    }
}
