<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "            
            CREATE TABLE `artists` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `public_name` VARCHAR(255) NOT NULL DEFAULT 'ID' COLLATE 'utf8mb4_unicode_ci',
                `description` TEXT NULL COLLATE 'utf8mb4_unicode_ci',
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB;
        ";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
