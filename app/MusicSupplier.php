<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicSupplier extends Model
{
    public function getArtistUrl()
    {
        $artistUrl = str_replace(
            ['{channel_id}', '{base_url}'],
            [$this->channels->id_on_supplier_side, $this->base_url],
            $this->artist_url_pattern
        );

        return 'https://' . $artistUrl;
    }

    public function getTrackUrl()
    {
        $trackUrl = str_replace(
            ['{track_id}', '{base_url}', '{channel_id}'],
            [$this->id_on_supplier_side, $this->base_url, $this->channel_alias],
            $this->track_url_pattern
        );

        return 'https://' . $trackUrl;
    }
}
