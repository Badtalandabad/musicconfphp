<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    public function artists()
    {
        return $this->belongsToMany('App\\Artist', 'tracks_artists_map')
            ;//->as('tracksMap');
    }

    public function musicSuppliers()
    {
        return $this->belongsToMany(
                'App\\MusicSupplier',
                'music_suppliers_tracks_map',
                null,
                'supplier_id')
            ->join(
                'channels',
                'music_suppliers_tracks_map.channel_id',
                '=',
                'channels.id')
            ->select(
                'music_suppliers_tracks_map.*',
                'music_suppliers.track_url_pattern',
                'music_suppliers.name',
                'music_suppliers.base_url',
                'channels.id_on_supplier_side AS channel_alias')
            ->as('tracksMap');
    }
}
