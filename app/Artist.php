<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public function tracks()
    {
        return $this->belongsToMany('App\\Track', 'tracks_artists_map')
            ;//->as('artistsMap');
    }

    public function musicSuppliers()
    {
        return $this->belongsToMany(
                'App\\MusicSupplier',
                'channels',
                null,
                'supplier_id'
            )->withPivot('id_on_supplier_side')
            ->as('channels');
    }
}
