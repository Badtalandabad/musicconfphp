<?php

namespace App\Http\Controllers;

use App\MusicSupplier;
use Illuminate\Http\Request;

class MusicSuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MusicSupplier  $musicSupplier
     * @return \Illuminate\Http\Response
     */
    public function show(MusicSupplier $musicSupplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MusicSupplier  $musicSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit(MusicSupplier $musicSupplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MusicSupplier  $musicSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MusicSupplier $musicSupplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MusicSupplier  $musicSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(MusicSupplier $musicSupplier)
    {
        //
    }
}
