@extends('layouts.app')

@section('content')
    @if($tracks)
        <ul>
            @foreach($tracks as $track)
                <li>{{$track->name}}</li>
            @endforeach
        </ul>
    @endif
@endsection
