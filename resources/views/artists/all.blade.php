@extends('layouts.app')

@section('content')
    @if($artists)
        <ul>
        @foreach($artists as $artist)
            <li>
                {{$artist->public_name}}
                @if($artist->musicSuppliers)
                    @foreach($artist->musicSuppliers as $supplier)
                        | <a href="{{$supplier->getArtistUrl()}}" target="_blank">{{$supplier->name}}</a>
                    @endforeach
                @endif
                @if($artist->tracks)
                    <ul>
                    @foreach($artist->tracks as $track)
                        <li>
                            {{$track->full_name}}
                            @if($track->musicSuppliers)
                                @foreach($track->musicSuppliers as $supplier)
                                    | <a href="{{$supplier->getTrackUrl()}}" target="_blank">{{$supplier->name}}</a>
                                @endforeach
                            @endif
                        </li>
                    @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
        </ul>
    @endif
@endsection
